<?php
class Itec_SupplierDashboard_Helper_Data extends Mage_Core_Helper_Abstract
{

    CONST ITOKEN = Itec_SupplierDashboard_Model_Observer::ITOKEN;
    CONST FEED_SOURCE = Itec_SupplierDashboard_Model_Observer::FEED_SOURCE;
    CONST SHIPMENT_CREATE_URL = Itec_SupplierDashboard_Model_Observer::SHIPMENT_CREATE_URL;
    CONST SHIPMENT_UPDATE_URL = Itec_SupplierDashboard_Model_Observer::SHIPMENT_UPDATE_URL;

    public function sendNewShipment($shipment)
    {
        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;

        $data['items'] = array();
        $data['created_at'] = $shipment->getCreatedAt();

        $order = Mage::getModel('sales/order')->load($shipment->getOrderId());
        $shipment2 = Mage::getModel('sales/order_shipment')->load($shipment->getOrderId());
        // echo '<pre>';
        // var_dump($order);
        // var_dump($shipment->total_qty);
        // var_dump($order->total_qty_ordered);
        // echo '</pre>';
        //die();
        if ($order->getAssignationStock())
            {
                $dataAssignation = Mage::helper('core')->jsonDecode($order->getAssignationStock());
                $places_collection = Mage::getModel('pointofsale/pointofsale')->getCollection()->setOrder('position', 'ASC');
                $order_items = Mage::helper('advancedinventory')->getOrderedItems($order);
                $transmission = '';
                foreach($dataAssignation as $item_id => $item){
                    foreach($places_collection as $place){
                        if (isset($item[$place->getPlaceId()])){
                            if ($item[$place->getPlaceId()] > 0){
                                {
                                    if($place->getName() == 'PANALPINA'){
                                        $ordered_items = $order->getAllItems();
                                        foreach($ordered_items as $position){
                                            if($item_id == $position->getProductId()){
                                                $qtyForShipping = $_POST['shipment']['items'][$order_items[$item_id]['item_id']];  //Сколько отправляем сейчас
                                                $totalProductQ = $position->getQtyOrdered();                                        //Сколько всего заказано
                                                $alreadyShipped = $order_items[$item_id]['qty_transmitted'];                        //Сколько уже заказали
                                                //var_dump((int)$qtyForShipping + (int)$alreadyShipped == (int)$totalProductQ);
                                                if((int)$qtyForShipping + (int)$alreadyShipped == (int)$totalProductQ)
                                                    $transmission = 'Fully transmitted';
                                                else{
                                                    $transmission = 'Partially transmitted';
                                                }
                                            }
                                            
                                        }
                                    } 
                                }
                                
                            }
                        }
                    }
                }
            }

            

        
        $data['order_id'] = $order->getIncrementId();
        $data['transmission'] = $transmission;
        $itemsCollection = $shipment->getItemsCollection();
        // Mage::log('New Shipment ' . var_export($shipment->getData(), 1), null, 'SupplierDashboard.log');
        // Mage::log('New Shipment current $itemsCollection' . var_export($itemsCollection->getData(), 1), null, 'SupplierDashboard.log');

        foreach($itemsCollection as $item)
        {
            $data['items'][$item->getSku()] = [
                'sku'           => $item->getSku(),
                'qty'           => $item->getQty()
            ];
        }
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::SHIPMENT_CREATE_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            if (!empty($out)){
                Mage::log('FEED_SHIPMENT_UPDATE DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
            }else{

            }

            curl_close($curl);
        }
        else{
            // echo 'ERROR';
            // send an email
        }
    }

    public function sendTrackings($shipment, $trackData)
    {
        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;

        $data['carrier'] = '';
        $data['tracknumber'] = '';

        $order = Mage::getModel('sales/order')->load($shipment->getOrderId());
        $data['order_id'] = $order->getIncrementId();

        $data['items'] = array();
        $itemsCollection = $shipment->getItemsCollection();

        foreach($itemsCollection as $item)
        {
            $data['items'][] = $item->getSku();
        }

        if (count($trackData) > 1)
        {
            $data['carrier'] = 'ERROR';
            $data['tracknumber'] = 'ERROR';
        }
        else
        if (count($trackData) == 1)
        {
            $data['carrier'] = $trackData[0]['title'];
            $data['tracknumber'] = $trackData[0]['track_number'];
        }

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::SHIPMENT_UPDATE_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            if (!empty($out)){
                Mage::log('FEED_SHIPMENT_UPDATE DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
            }else{

            }

            curl_close($curl);
        }
        else{
            // echo 'ERROR';
            // send an email
        }
    }
}
