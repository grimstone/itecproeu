<?php
class Itec_SupplierDashboard_Model_Observer {

    const FEED_SOURCE = 'ITEC-TEST-5';
    const ITOKEN = 'lsYtKjzGDDYHHB9FHjsrxiY5z2aIlOIF';
    const CREATE_URL = 'http://supplier.itec-ovh.ml/feed/magento-create';
    const CANCEL_URL = 'http://supplier.itec-ovh.ml/feed/magento-cancel';
    const UPDATE_URL = 'http://supplier.itec-ovh.ml/feed/magento-update';
    const STOCK_UPDATE_URL = 'http://supplier.itec-ovh.ml/feed/magento-stock-update';
    const STOCK_DELETE_URL = 'http://supplier.itec-ovh.ml/feed/magento-stock-delete';
    const COMPANY_UPDATE_URL = 'http://supplier.itec-ovh.ml/feed/magento-seller-update';
    const SHIPMENT_CREATE_URL = 'http://supplier.itec-ovh.ml/feed/magento-shipment-create';
    const SHIPMENT_UPDATE_URL = 'http://supplier.itec-ovh.ml/feed/magento-shipment-update';
    // const EXCLUDE_EMAIL = ['jean.simler@comexholdings.com', 'jean.simler@gmail.com', 'julie.vicart@comexholdings.com'];
    const EXCLUDE_EMAIL = [];


	public function send_order($observer) {

        if (in_array(strtolower($observer->getEvent()->getOrder()->getCustomerEmail()), self::EXCLUDE_EMAIL)) return false;

        $order_id = $observer->getEvent()->getOrder()->getIncrementId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
        //get customer's information
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        //$shipping_data = $order->getBillingAddress()->getData();
        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;

        $data['iref'] = $order_id;
        $data['buyer_country'] = $order->getBillingAddress()->getCountry();
        $data['date_ordered'] = $order->getCreatedAt();
        //$data['buyer'] = $order->getCustomerName(); //Add customer name to buyer field
        $data['buyer'] = $customer['company_name']; //Add company name to buyer field
        $data['payment_method'] = $order->getPayment()->getMethodInstance()->getCode();
        switch ($data['payment_method']) {
            case 'banktransfer':
                $data['payment_method'] = '5';
                break;

            case 'cashondelivery':
                $data['payment_method'] = '60';
                break;

            case 'paypal':
            case 'paypal_express':
            case 'comnpay_debit':
                $data['payment_method'] = 'cc';
                break;

            default:
                # code...
                break;
        }

        $data['magento_currency'] = $order->getOrderCurrencyCode();
        $data['tax_percent'] = 0;
        $data['shipping_amount'] = floatval($order->getShippingAmount());
        $data['discount_amount'] = floatval($order->getDiscountAmount());
        $data['discount_description'] = $order->getDiscountDescription();

        $orderItems = $order->getItemsCollection();

        $arrItems = [];
        foreach ($orderItems as $item) {
            if ($item->getParentItemId() != NULL)
                {
                    $_product = Mage::getModel('catalog/product')->setStoreId(1)->load($item->getProductId());
                    $arrItems[$item->getParentItemId()] = $_product->getName();
                }
        }

        foreach ($orderItems as $item){

            if ($item->getParentItemId() != NULL)
                {
                    // var_dump($productName);
                    continue;
                }

            $_product = Mage::getModel('catalog/product')->setStoreId(1)->load($item->getProductId());

            if($_product->isConfigurable()){
                $productName = $arrItems[$item->getItemId()];
            }else{
                $productName = $_product->getName();
            }
                
            $row_total = $item->getRowTotal() + $item->getTaxAmount() + $item->getHiddenTaxAmount() + Mage::helper('weee')->getRowWeeeAmountAfterDiscount($item) - $item->getDiscountAmount();
            $stock = Mage::app()->setCurrentStore(1)->getLayout()->createBlock('advancedinventory/stocks')->getStockQty($_product);

            if ($item->getTaxPercent() > 0)
            {
                $data['tax_percent'] = $item->getTaxPercent();
            }

            $price_total = floatval($item->getPrice()) * (int)$item->getQtyOrdered();
            $vat = $price_total*floatval($item->getTaxPercent())/100;



            $data['items'][] = [
                'product_name' => $productName,
                'iar' => $item->getSku(),
                'description' =>  $_product->getSar(),
                'seller' => $_product->getCompany(),
                'quantity_ordered' => $item->getQtyOrdered(),
                'unit_price' => $item->getPrice(),
                'row_total' => $row_total,
                'quantity' => (int)$stock - (int)$item->getQtyOrdered(),

                'magento_total_refunded' => $item->getQtyRefunded(),
                'magento_vat' => $vat,
                'magento_price_and_vat' => $price_total + $vat,
                'qty_invoiced' => 0,
            ];


        }
        //die(var_dump($data));

        // Mage::log('Observer new order: ' . var_export($data['items'], 1), null, 'SupplierDashboard.log');

        /* !!!!!!!!!!!!!!!!!!!
        IN DASHBOARD YOU MUST FIND THE SELLER BY IAR CODE (first 4 letters)
        if it doesn't exist -> add new seller in Supplier Dashboard
        */

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::CREATE_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            if (!empty($out)){
                // Mage::log('FEED DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
            }

            curl_close($curl);
        }
        else{
            // echo 'ERROR';
            // send an email
        }

    }

    public function update_order($observer)
    {
        $order_id = $observer->getEvent()->getOrder()->getIncrementId();

        // if(!Mage::registry('prevent_observer-update_order' . $order_id))
        // {
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);

        // update event is fired several time
        // so we avoid unnecessary trigger
        if ($order->getStatus() != NULL && $order->getStatus() != 'new')
        {
            $data = [];
            $data['feed_source'] = self::FEED_SOURCE;
            $data['itoken'] = self::ITOKEN;
            $data['iref'] = $order_id;

            $data['total_due'] = $order->getTotalDue();
            $data['total_paid'] = $order->getTotalPaid();
            $data['order_status'] = $order->getStatus();

            $orderItems = $order->getItemsCollection();

            // Get warehouses
            if ($order->getAssignationStock())
            {
                $dataAssignation = Mage::helper('core')->jsonDecode($order->getAssignationStock());
                $places_collection = Mage::getModel('pointofsale/pointofsale')->getCollection()->setOrder('position', 'ASC');
                $order_items = Mage::helper('advancedinventory')->getOrderedItems($order);

                $assignation = array();
                foreach($dataAssignation as $item_id => $item){
                    foreach($places_collection as $place){
                        if (isset($item[$place->getPlaceId()])){
                            if ($item[$place->getPlaceId()] > 0){
                                {
                                    if($place->getName() == 'VP PANA')
                                        $assignation[$item_id]['warehouses'][] = "PANA";
                                    else if($place->getName() == 'VP METZ')
                                        $assignation[$item_id]['warehouses'][] = "METZ";
                                    else
                                        $assignation[$item_id]['warehouses'][] = $place->getName();
                                }
                                
                                //$place->getName() == 'VP PANA' ? $assignation[$item_id]['warehouses'][] = "PANA" : $assignation[$item_id]['warehouses'][] = $place->getName() ;
                            }
                        }
                    }
                }
                foreach($assignation as $item_id => $assign)
                {
                    $assignation[$item_id]['warehouses'] = count($assignation[$item_id]['warehouses']) > 1 ? 'ERROR' : $assignation[$item_id]['warehouses'][0];
                }

                // adjust the $assignation array
                foreach($orderItems as $item)
                {
                    if ($item->getParentItemId() != NULL)
                    {
                        $parentIds = Mage::getModel('catalog/product_type_configurable')
                            ->getParentIdsByChild($item->getProductId());

                        $assignation[$parentIds[0]] = $assignation[$item->getProductId()];
                    }
                }

                // Mage::log('FEED_UPDATE_ORDER $assignation: ' . var_export($assignation, 1), null, 'SupplierDashboard.log');
            }

            foreach ($orderItems as $item){

                if ($item->getParentItemId() != NULL) continue;

                $_product = Mage::getModel('catalog/product')->setStoreId(1)->load($item->getProductId());
                $row_total = $item->getRowTotal() + $item->getTaxAmount() + $item->getHiddenTaxAmount() + Mage::helper('weee')->getRowWeeeAmountAfterDiscount($item) - $item->getDiscountAmount();
                $stock = Mage::app()->setCurrentStore(1)->getLayout()->createBlock('advancedinventory/stocks')->getStockQty($_product);

                $data['items'][$item->getSku()] = [
                    'product_name' => $_product->getName(),
                    'iar' => $item->getSku(),
                    'description' =>  $_product->getSar(),
                    'seller' => $_product->getCompany(),
                    'quantity_ordered' => $item->getQtyOrdered(),
                    'unit_price' => $item->getPrice(),
                    'row_total' => $row_total,
                    'quantity' => (int)$stock - (int)$item->getQtyOrdered(),

                    'refunded' => $item->getQtyRefunded(),
                    // 'vat' => $item->getTaxAmount(),
                    // 'price_and_vat' => floatval($item->getPrice()) * (int)$item->getQtyOrdered() + floatval($item->getTaxAmount()),
                    'qty_invoiced' => 0,
                    'warehouses' => isset($assignation[$item->getProductId()]) ? $assignation[$item->getProductId()]['warehouses'] : '',
                ];

                if ($item->getTaxPercent() > 0)
                {
                    $data['tax_percent'] = $item->getTaxPercent();
                }

                // Mage::log('FEED_UPDATE_ORDER DASHBOARD RESPONSE: ' . var_export($item->getData(), 1), null, 'SupplierDashboard.log');
            }

            if ($order->hasInvoices()) {
                foreach ($order->getInvoiceCollection() as $invoice) {
                    foreach ($invoice->getAllItems() as $item)
                    {
                        // strange but if an item is not invoiced -> qty = qty ordered (but not 0)
                        // so we check the price
                        if ($item->getPrice() > 0)
                        {
                            $data['items'][$item->getSku()]['qty_invoiced'] += $item->getQty();
                        }
                    }
                }
            }


            // Mage::log('FEED_UPDATE_ORDER DASHBOARD RESPONSE: ' . var_export($data, 1), null, 'SupplierDashboard.log');
            // Mage::log('FEED_UPDATE_ORDER DASHBOARD RESPONSE: ' . var_export($logisticdata, 1), null, 'SupplierDashboard.log');

            if( $curl = curl_init() ) {
                curl_setopt($curl, CURLOPT_URL, self::UPDATE_URL);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                $out = curl_exec($curl);

                if (!empty($out)){
                    Mage::log('FEED_UPDATE_ORDER DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
                }

                curl_close($curl);
            }
            // Mage::log('Observer new order: ' . var_export($order->getTotalDue(), 1), null, 'SupplierDashboard.log');
        }

        // Mage::register('prevent_observer-update_order' . $order_id, true);
        // }
    }

    public function cancel_order($observer)
    {
        if (in_array(strtolower($observer->getEvent()->getOrder()->getCustomerEmail()), self::EXCLUDE_EMAIL)) return false;

        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;

        $order_id = $observer->getEvent()->getOrder()->getIncrementId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);

        $data['iref'] = $order_id;

        $orderItems = $order->getItemsCollection();
        foreach ($orderItems as $item){

            if ($item->getParentItemId() != NULL) continue;

            $_product = Mage::getModel('catalog/product')->setStoreId(1)->load($item->getProductId());
            $stock = Mage::app()->setCurrentStore(1)->getLayout()->createBlock('advancedinventory/stocks')->getStockQty($_product);

            $data['items'][] = [
                'iar' => $item->getSku(),
                'quantity_ordered' => $item->getQtyOrdered(),
                'quantity' => $stock, // correct. not need to add the qty_ordered
            ];
        }
        // Mage::log('Observer new order: ' . var_export($data['items'], 1), null, 'SupplierDashboard.log');

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::CANCEL_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            if (!empty($out)){
                Mage::log('FEED_CANCEL DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
            }

            curl_close($curl);
        }
        else{
            // echo 'ERROR';
            // send an email
        }

        // Mage::log('Observer canceled order: ' . var_export($order->getData(), 1), null, 'mylogfile.log');
    }

    public function product_change($observer)
    {
        $product = Mage::getModel('catalog/product')->load($observer->getEvent()->getProduct()->getId());
        // $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        $stock = Mage::app()->setCurrentStore(1)->getLayout()->createBlock('advancedinventory/stocks')->getStockQty($product);

        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;

        $data['iar'] = $product->getSku();
        $data['sar'] = $product->getSar();
        $data['product_name'] = $product->getName();
        $data['min_qty'] = $product->getMinQty();
        $data['qty'] = $stock;
        // $data['min_qty'] = $product->getMinQty();
        $data['unit_price'] = $product->getPrice();


        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::STOCK_UPDATE_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            if (!empty($out)){
                Mage::log('FEED_PRODUCT_CHANGE DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
            }

            curl_close($curl);
        }
        else{
            // echo 'ERROR';
            // send an email
        }
    }

    public function product_delete($observer)
    {
        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;

        $data['iar'] = $observer->getEvent()->getProduct()->getSku();

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::STOCK_DELETE_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            if (!empty($out)){
                Mage::log('FEED_PRODUCT_DELETE DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
            }

            curl_close($curl);
        }
        else{
            // echo 'ERROR';
            // send an email
        }
    }

    public function company_save($observer)
    {
        $company = $observer->getEvent()->getCompany();
        // Mage::log('Observer Companies save: ' . var_export($company, 1), null, 'SupplierDashboard.log');

        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;
        $data['seller'] = $company;

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::COMPANY_UPDATE_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            if (!empty($out)){
                Mage::log('FEED_SELLER_UPDATE DASHBOARD RESPONSE: ' . var_export($out, 1), null, 'SupplierDashboard.log');
                Mage::getSingleton('adminhtml/session')->addError('Error on Supplier Dashboard. ' . $out);
            }else{
                Mage::getSingleton('adminhtml/session')->addSuccess('Successfully updated on Supplier Dashboard');
            }

            curl_close($curl);
        }
        else{
            // echo 'ERROR';
            // send an email
        }
    }

    public function create_shipment($observer)
    {
        $data['feed_source'] = self::FEED_SOURCE;
        $data['itoken'] = self::ITOKEN;
        $shipment = Mage::getModel('sales/order_shipment')->load($observer->getEvent()->getShipment()->getId());

        $trackings=Mage::getResourceModel('sales/order_shipment_track_collection')->addAttributeToSelect('*')->addAttributeToFilter('parent_id',$shipment->getId());
        $trackings->getSelect()->order('entity_id desc')->limit(2);

        $trackData = $trackings->getData();
        // $trackID = $trackData[0]['entity_id'];
        // $trackNumber = $trackData[0]['track_number'];
        // $trackTitle = $trackData[0]['title'];
        // $trackCarrier = $trackData[0]['carrier_code'];

        // Mage::log('New Shipment ' . var_export($trackData, 1), null, 'SupplierDashboard.log');

        // if it is a new shipment
        if ($shipment->getCreatedAt() == $shipment->getUpdatedAt())
        {   
            // echo '<pre>';
            // die(var_dump(Mage::registry('current_shipment') !== null));
            // echo '</pre>';
            //if is not shipment for PANA. [Panalpina shipment is Mage::registry('current_shipment_pana')]
            //die(var_dump(Mage::registry('current_shipment') !== null));
            if (Mage::registry('current_shipment') !== null || Mage::registry('current_shipment_pana') !== null)
            {
                Mage::helper('itec_supplierdashboard')->sendNewShipment($shipment);
            }
           
            // if(Mage::registry('current_shipment_pana') !== null)
            // {
                
            //     die(var_dump($shipment));
            // }
        }
        else
        {
            // updating. send track number
        }

    }

    public function create_track($observer)
    {
        $track = $observer->getEvent()->getTrack();
        $_shipment = $track->getShipment();
        // $order = $_shipment->getOrder();

        $shipment = Mage::getModel('sales/order_shipment')->load($_shipment->getId());

        $trackings=Mage::getResourceModel('sales/order_shipment_track_collection')->addAttributeToSelect('*')->addAttributeToFilter('parent_id',$shipment->getId());
        $trackings->getSelect()->order('entity_id desc')->limit(2);

        $trackData = $trackings->getData();

        Mage::helper('itec_supplierdashboard')->sendTrackings($shipment, $trackData);

    }
}
