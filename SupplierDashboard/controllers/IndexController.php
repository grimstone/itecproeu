<?php

class Itec_SupplierDashboard_IndexController extends Mage_Core_Controller_Front_Action
{
    CONST ITOKEN = Itec_SupplierDashboard_Model_Observer::ITOKEN;

    function preDispatch()
    {
        parent::preDispatch();

        if ($_POST['itoken'] !== self::ITOKEN)
        {
            http_response_code(403);
            echo 'Forbidden';
            exit;
        }
   }

    public function update_sellerAction()
    {
        $company = Mage::getModel('iteccompanies/companies')->load($_POST['user_code'], 'sku');

        if ($company->getId())
        {
            $company->password = $_POST['password'];
            $company->save();
        }
        else
        {
            Mage::log('MAGENTO UPDATE SELLER: Company with reference ' . $_POST['user_code'] . ' doesn\'t exist', null, 'SupplierDashboard.log');
            echo 'Not found';
        }
    }

    public function updateallsellersAction()
    {

        // foreach (Mage::getModel('iteccompanies/companies')->getCollection() as $news) {
        //
        //     if (isset($_POST['sellers'][$news->getSku()]))
        //     {
        //         try {
        //             $news->load($news->getId())->setUsername($_POST['sellers'][$news->getSku()]['username'])->save();
        //         } catch (Exception $e) {
        //             //$news->setId($news->getId())->setLink($news->getId())->save();
        //             Mage::log('MAGENTO UPDATE All SELLER' . $e->getMessage(), null, 'SupplierDashboard.log');
        //         }
        //
        //     }
        //
        // }

    }

    public function test_sellerAction()
    {
        $newsId = Mage::app()->getRequest()->getParam('id', 0);
        $company = Mage::getModel('iteccompanies/companies')->load($newsId);

        if ($company->getId() > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('companies.content')->assign(array(
                "companyItem" => $company,
            ));
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }

    public function getordersAction(){
        // получить все заказы и передать
        $salesModel=Mage::getModel("sales/order");
        $salesCollection = $salesModel->getCollection();
        $companyCustomerArr = [];

        foreach($salesCollection as $order)
        {
            $orderId= $order->getIncrementId();
            $orderCustomer = $order->getCustomerName();
            $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
            $customerEl = ['id' => $orderId, 'name' => $orderCustomer, 'company' => $customer['company_name']];
            array_push($companyCustomerArr, $customerEl);
        }
        echo serialize($companyCustomerArr);
    }

}
